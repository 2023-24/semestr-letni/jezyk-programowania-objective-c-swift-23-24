# Języki programowania Objective-C/Swift

## Uniwersytet Merito w Poznaniu - Studia I stopnia - Kierunek Informatyka

### Informacje wstępne

Kod znajdujący się w repozytorium został przygotowany jako wsparcie dla zajęć realizowanych na Uniwersytecie Merito w Poznaniu, w ramach Studiów I stopnia na Kierunku Informatyka.

Materiał powinien być wykorzystywany w połączeniu z dodatkowymi wyjaśnieniami, na przykład ze strony Prowadzącego zajęcia.

Kod zawarty w repozytorium służy tylko i wyłącznie do celom edukacyjnych. W szczególności nie powinien być wykorzystywany produkcyjnie z uwagi na zawarte w nim uproszczenia.



### Źródła

Szczegółowe odwołania do źródeł, na bazie których powstawały załączone przykłady, znaleźć można na platformie Moodle oraz w materiałach prezentowanych w trakcie zajęć.

Poniżej wybrane źródła:
* https://developer.apple.com/tutorials/swiftui/creating-and-combining-views, Apple Inc., 2024.
* https://developer.apple.com/tutorials/swiftui/building-lists-and-navigation, Apple Inc., 2024.
* https://developer.apple.com/xcode/swiftui/, Apple Inc., 2024.
* https://www.hackingwithswift.com/quick-start/swiftui/what-is-swiftui, Paul Hudson, 2021.
* https://www.hackingwithswift.com/quick-start/swiftui/answering-the-big-question-should-you-learn-swiftui-uikit-or-both, Paul Hudson, 2021.
* https://www.bluelabellabs.com/blog/swiftui-vs-uikit/, Bobby Gill, Blue Label Labs, 2022.
* https://developer.apple.com/tutorials/app-dev-training/creating-a-list-view, Apple Inc., 2024.
